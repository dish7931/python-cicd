import unittest
from hello_python import hello

class TestHelloPython(unittest.TestCase):
    def test_hello_python(self):
        self.assertEqual(hello(), 'Hello, Python!')

if __name__ == '__main__':
    unittest.main()
